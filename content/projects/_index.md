---
title: "Projects"
hero_image: "hero.jpg"
nometadata: true
notags: true
noshare: true
nocomments: true
---

<h2>CogNap</h2>
Le projet [CogNap](http://www.cognap.uliege.be/) vise à explorer le lien entre régulation circadienne du cycle de veille-sommeil et le vieillissement cognitif et cérébral.
