---
title: "A beginning"
hero_image: "hero.jpg"
date: 2018-10-30T16:44:36-07:00
description: "My first blog post."
---

<h2>A beginning is the time for taking the most delicate care that the balances are correct.</h2>
Like many of us, I've been thinking for a while about sharing my current and past research projects, my research interests or even simple ideas. Let's see where it will lead me/us.
