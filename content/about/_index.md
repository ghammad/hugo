---
title: "About"
hero_image: "hero.jpg"
nometadata: true
notags: true
noshare: true
nocomments: true
---

<br>
Elementary particle physicist, now working in neurosciences... More on this later.
